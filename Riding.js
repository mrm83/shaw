var Riding = function(data) {
  this.data = data;
  this.render();
};

Riding.prototype.render = function() {
  this.drawRiding();
  this.drawResults();
};

Riding.prototype.drawRiding = function() {
  var active = this.data.num == 1 ? "active" : "";
  var name = this.data.name;
  $('.carousel-inner').append(
    "<div class='item riding_" + this.data.num + " " + active + "'>" +
      "<h3 class='name'>" + this.data.name + "</h3>" +
      "<ol class='results'></ol>" +
    "</div>"
  );
};

Riding.prototype.drawResults = function() {
  var totalVotes = 0;
  for (var i = 0; i < this.data.results.length; i++) {
    totalVotes += this.data.results[i].votes;
  }

  for (var i = 0; i < this.data.results.length; i++) {
    var elected = this.data.results[i].isElected ? " elected " : " ";
    var votes = this.data.results[i].votes;
    var percent = Math.round(votes/totalVotes*100);
    $('.riding_' + this.data.num + ' .results').append(
      "<li class='candidate" + elected + this.data.results[i].partyCode + "'>" +
        "<span class='name'>" + this.data.results[i].name + "</span>" +
        "<span class='votes'>" + votes + " votes (<span class='total'>" + percent + "</span>%)</span>" +
      "</li>"
    );
  }
};
